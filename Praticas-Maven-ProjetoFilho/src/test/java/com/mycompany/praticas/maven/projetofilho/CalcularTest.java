/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.praticas.maven.projetofilho;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NandaPC
 */
public class CalcularTest {
    
    public CalcularTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of soma method, of class Calcular.
     */
    @org.junit.Test
    public void testSoma() {
        System.out.println("soma");
        
        Calcular instance = new Calcular();
        assertEquals(5, instance.soma(2, 3));
        assertEquals(8, instance.soma(3, 5));
       
    }
    
}
